'use strict';

var path = require('path');

module.exports = {
  build: {
    env: require('./prod.env'),

    index: path.resolve(__dirname, '../../public/index.html'),

    assetsRoot: path.resolve(__dirname, '../../public'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    productionSourceMap: true,

    productionGzip: false,
    productionGzipExtensions: ['js', 'css']
  },
  dev: {
    env: require('./dev.env'),
    port: 8080,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true
      }
    },

    cssSourceMap: false
  }
};

//# sourceMappingURL=index-compiled.js.map