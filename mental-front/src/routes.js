import auth from './auth/auth.js';
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Signup from './components/Signup.vue'
import User from './components/user/User.vue'
import UserOptions from './components/user/UserOptions.vue'
import UserOptionsPrimary from './components/user/UserOptionsPrimary.vue'
import UserOptionsSecurity from './components/user/UserOptionsSecurity.vue'
import UserOptionsExtra from './components/user/UserOptionsExtra.vue'
import UserFriends from './components/user/Users.vue'
import Admin from './components/admin/Admin.vue'
import AdminDashboard from './components/admin/Dashboard.vue'
import AdminUsers from './components/admin/Users.vue'
import AdminCourses from './components/admin/Courses.vue'
import AdminTeachRequests from './components/admin/TeachRequests.vue'
import Courses from './components/courses/Courses.vue'
import Course from './components/courses/Course.vue'
import Teacher from './components/teacher/Teacher.vue'
import TeacherDashboard from './components/teacher/Dashboard.vue'
import TeacherCourses from './components/teacher/Courses.vue'
import TeacherCourse from './components/teacher/Course.vue'
import TeacherMental from './components/teacher/Mental.vue'
import TeacherStudents from './components/teacher/Students.vue'
import Student from './components/desktop/Student.vue'
import Game from './components/mental/Game.vue'
import GameSimple from './components/mental/GameSimple.vue'

export const routes = [
  { path: '',
    name: 'Home',
    components: {
      default: Home
    } },
  { path: '/login',
    name: 'Login',
    components: {
      default: Login
    } },
  { path: '/signup',
    name: 'Signup',
    components: {
      default: Signup
    } },
  { path: '/admin',
    name: 'Admin',
    components: {
      default: Admin
    },
    beforeEnter: auth.requireAdmin,
    meta: { auth: true },
    children: [
      {
        path: 'dashboard',
        name: 'AdminDashboard',
        component: AdminDashboard
      },
      {
        path: 'users',
        name: 'AdminUsers',
        component: AdminUsers
      },
      {
        path: 'courses',
        name: 'AdminCourses',
        component: AdminCourses
      },
      {
        path: 'teachrequests',
        name: 'AdminTeachRequests',
        component: AdminTeachRequests
      }
    ]
  },
  { path: '/courses',
    name: 'Courses',
    components: {
      default: Courses
    }
  },
  {
    path: '/course/:id',
    name: 'Course',
    component: Course
  },
  {
    path: '/user/:id',
    name: 'User',
    component: User
  },
  {
    path: '/options',
    component: UserOptions,
    children: [
      {
        path: 'primary',
        name: 'Options',
        component: UserOptionsPrimary
      },
      {
        path: 'security',
        name: 'OptionsSecurity',
        component: UserOptionsSecurity
      },
      {
        path: 'extra',
        name: 'OptionsExtra',
        component: UserOptionsExtra
      }
    ]
  },
  {
    path: '/friends',
    name: 'Friends',
    component: UserFriends
  },
  {
    path: '/teacher',
    name: 'Teacher',
    component: Teacher,
    beforeEnter: auth.requireTeacher,
    children: [
      {
        path: 'dashboard',
        name: 'TeacherDashboard',
        component: TeacherDashboard
      },
      {
        path: 'courses',
        name: 'TeacherCourses',
        component: TeacherCourses
      },
      {
        path: 'courses/:id',
        name: 'TeacherCourse',
        component: TeacherCourse
      },
      {
        path: 'courses/mental',
        name: 'TeacherMental',
        component: TeacherMental
      },
      {
        path: 'students',
        name: 'TeacherStudents',
        component: TeacherStudents
      }
    ]
  },
  {
    path: '/student',
    name: 'Student',
    component: Student
  },
  {
    path: '/game',
    name: 'Game',
    component: Game,
    children: [
      {
        path: 'simple',
        name: 'GameSimple',
        component: GameSimple
      }
    ]
  },
  // { path: '/platform',
  //   name: 'Platform',
  //   components: {
  //     default: Platform
  //   },
  //   meta: { auth: true },
  //   children: [
  //
  //   ]
  // },
  { path: '*', redirect: '/' }
];
