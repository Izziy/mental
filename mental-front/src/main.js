// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueCookie from 'vue-cookie'
import Vuetify from 'vuetify'
//import BootstrapVue from 'bootstrap-vue'

import App from './App'
import { routes } from './routes'
import store from './store/store'
import auth from './auth/auth.js'
//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'
//import 'vuetify/dist/vuetify.min.css'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueCookie);
Vue.use(Vuetify);
//Vue.use(BootstrapVue);

Vue.http.headers.common['Authorization'] = auth.getAuthHeader();

export const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return {x: 0, y: 0};
  }
});

/*router.beforeEach( (to,from,next) => {
  if (!store.getters.user.authenticated) {
    if (localStorage.user && localStorage.id_token) {
      let user = JSON.parse(localStorage.user);
      store.dispatch('setUser',user);
      store.dispatch('authenticateUser');
    }
    if (to.matched.some(record => record.meta.auth) && !store.getters.user.authenticated) {
      next('login');
    } else {
      next()
    }
  } else {
    next()
  }
});*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
