'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.router = undefined;

var _vue = require('vue');

var _vue2 = _interopRequireDefault(_vue);

var _vueRouter = require('vue-router');

var _vueRouter2 = _interopRequireDefault(_vueRouter);

var _vueResource = require('vue-resource');

var _vueResource2 = _interopRequireDefault(_vueResource);

var _vueCookie = require('vue-cookie');

var _vueCookie2 = _interopRequireDefault(_vueCookie);

var _bootstrapVue = require('bootstrap-vue');

var _bootstrapVue2 = _interopRequireDefault(_bootstrapVue);

var _App = require('./App');

var _App2 = _interopRequireDefault(_App);

var _routes = require('./routes');

var _store = require('./store/store');

var _store2 = _interopRequireDefault(_store);

var _auth = require('./auth/auth.js');

var _auth2 = _interopRequireDefault(_auth);

require('bootstrap/dist/css/bootstrap.css');

require('bootstrap-vue/dist/bootstrap-vue.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_vue2.default.use(_vueRouter2.default);
_vue2.default.use(_vueResource2.default);
_vue2.default.use(_vueCookie2.default);
_vue2.default.use(_bootstrapVue2.default);

_vue2.default.http.headers.common['Authorization'] = _auth2.default.getAuthHeader();

var router = exports.router = new _vueRouter2.default({
  routes: _routes.routes,
  mode: 'history',
  scrollBehavior: function scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  }
});

router.beforeEach(function (to, from, next) {
  if (!_store2.default.getters.user.authenticated) {
    if (localStorage.user && localStorage.id_token) {
      var user = JSON.parse(localStorage.user);
      _store2.default.dispatch('setUser', user);
      _store2.default.dispatch('authenticateUser');
    }
    if (to.matched.some(function (record) {
      return record.meta.auth;
    }) && !_store2.default.getters.user.authenticated) {
      next('login');
    } else {
      next();
    }
  } else {
    next();
  }
});

new _vue2.default({
  el: '#app',
  router: router,
  store: _store2.default,
  template: '<App/>',
  components: { App: _App2.default }
});

//# sourceMappingURL=main-compiled.js.map