'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  randomNumber: function randomNumber(min, max) {
    var rand = min + Math.random() * (max - min);
    rand = Math.round(rand);
    return rand;
  },
  getSign: function getSign() {
    return Math.random() >= 0.5;
  },
  getNumber: function getNumber(capacity, max) {
    var number = 0;
    var kernel = 0;
    for (var i = 0; i < capacity; i++) {
      kernel = this.randomNumber(i == 0 ? 1 : 0, max);
      number += kernel;
    }
    return parseInt(number);
  },
  generateSimple: function generateSimple(passedBone, maxNumber, currentNumber) {
    var sign = true;
    var newNumber = '';
    var kernel = 0;
    var capacity = String(maxNumber).length;
    var currentNumberStr = String(currentNumber);
    var currentNumberLen = String(currentNumber).length;

    while (currentNumberLen < capacity) {
      currentNumberStr = '0' + currentNumberStr;
      currentNumberLen++;
    }

    if (currentNumber == 0) {
      sign = true;
    } else if (currentNumberStr[0] == '9') {
      sign = false;
    } else if (currentNumberStr[0] == '0') {
      sign = true;
    } else {
      sign = this.getSign();
    }

    for (var i = 0; i < capacity; i++) {
      var n = [];
      var current = parseInt(currentNumberStr[i]);
      if (current == 0) {
        n = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      } else if (current == 1) {
        n = [-1, 0, 1, 2, 3, 5, 6, 7, 8];
      } else if (current == 2) {
        n = [-2, -1, 0, 1, 2, 5, 6, 7];
      } else if (current == 3) {
        n = [-3, -2, -1, 0, 1, 5, 6];
      } else if (current == 4) {
        n = [-4, -3, -2, -1, 0, 5];
      } else if (current == 5) {
        n = [-5, 0, 1, 2, 3, 4];
      } else if (current == 6) {
        n = [-6, -5, -1, 0, 1, 2, 3];
      } else if (current == 7) {
        n = [-7, -6, -5, -2, -1, 0, 1, 2];
      } else if (current == 8) {
        n = [-8, -7, -6, -5, -3, -2, -1, 0, 1];
      } else if (current == 9) {
        n = [-9, -8, -7, -6, -5, -4, -3, -2, -1, 0];
      }


      for (var j = 0; j < n.length; j++) {
        if (Math.abs(n[j]) > passedBone) {
          n.splice(j, 1, -99);
        } else if (sign && n[j] < 0) {
          n.splice(j, 1, -99);
        } else if (!sign && n[j] > 0) {
          n.splice(j, 1, -99);
        }
      }

      while (n.indexOf(-99) >= 0) {
        n.splice(n.indexOf(-99), 1);
      }

      kernel = Math.abs(n[n.length == 1 ? 0 : this.randomNumber(0, n.length - 1)]);

      newNumber += kernel;
    }

    return sign ? parseInt(newNumber) : parseInt(newNumber) * -1;
  },
  generateSimpleSet: function generateSimpleSet(passedBone, maxNumber, maxCount) {
    var numberSet = [];
    var currentNumber = 0;
    var newNumber = 0;

    for (var i = 0; i < maxCount; i++) {
      newNumber = this.generateSimple(passedBone, maxNumber, currentNumber);

      while (currentNumber + newNumber > maxNumber || currentNumber + newNumber < 0) {
        newNumber = this.generateSimple(passedBone, maxNumber, currentNumber);
      }

      numberSet.push(newNumber);
      currentNumber += newNumber;
    }
    return {
      result: currentNumber,
      resultSet: numberSet
    };
  }
};

//# sourceMappingURL=mentalFunctions-compiled.js.map