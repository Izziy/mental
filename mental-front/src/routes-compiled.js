'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routes = undefined;

var _auth = require('./auth/auth.js');

var _auth2 = _interopRequireDefault(_auth);

var _Header = require('./components/Header.vue');

var _Header2 = _interopRequireDefault(_Header);

var _Home = require('./components/Home.vue');

var _Home2 = _interopRequireDefault(_Home);

var _Login = require('./components/Login.vue');

var _Login2 = _interopRequireDefault(_Login);

var _Signup = require('./components/Signup.vue');

var _Signup2 = _interopRequireDefault(_Signup);

var _MentalProfile = require('./components/mental/MentalProfile.vue');

var _MentalProfile2 = _interopRequireDefault(_MentalProfile);

var _Admin = require('./components/admin/Admin.vue');

var _Admin2 = _interopRequireDefault(_Admin);

var _Mental = require('./components/mental/Platform.vue');

var _Mental2 = _interopRequireDefault(_Mental);

var _MentalTeacher = require('./components/mental/DesktopTeacher.vue');

var _MentalTeacher2 = _interopRequireDefault(_MentalTeacher);

var _MentalStudent = require('./components/mental/MentalStudent.vue');

var _MentalStudent2 = _interopRequireDefault(_MentalStudent);

var _MentalGame = require('./components/mental/MentalGame.vue');

var _MentalGame2 = _interopRequireDefault(_MentalGame);

var _MentalOptions = require('./components/mental/UserOptions.vue');

var _MentalOptions2 = _interopRequireDefault(_MentalOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routes = exports.routes = [{ path: '',
  name: 'Home',
  components: {
    default: _Home2.default
  } }, { path: '/login',
  name: 'Login',
  components: {
    default: _Login2.default
  } }, { path: '/signup',
  name: 'Signup',
  components: {
    default: _Signup2.default
  } }, { path: '/admin',
  name: 'Admin',
  components: {
    default: _Admin2.default
  }, beforeEnter: _auth2.default.requireAdmin }, { path: '/platform',
  name: 'Platform',
  components: {
    default: _Mental2.default
  },
  auth: true,
  beforeEnter: _auth2.default.requireAuth,
  children: [{
    path: 'profile',
    name: 'Profile',
    component: _MentalProfile2.default
  }, {
    path: 'teacher',
    name: 'Teacher',
    component: _MentalTeacher2.default,
    beforeEnter: _auth2.default.requireTeacher
  }, {
    path: 'student',
    name: 'Student',
    component: _MentalStudent2.default
  }, {
    path: 'game',
    name: 'Game',
    component: _MentalGame2.default
  }, {
    path: 'options',
    name: 'Options',
    component: _MentalOptions2.default
  }]
}, { path: '*', redirect: '/' }];

//# sourceMappingURL=routes-compiled.js.map
