'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _store = require('../store/store');

var _store2 = _interopRequireDefault(_store);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var API_URL = '/api';
var LOGIN_URL = API_URL + '/login';
var SIGNUP_URL = API_URL + '/signup';

exports.default = {
  login: function login(context, creds, redirect) {
    context.$http.post(LOGIN_URL, creds).then(function (data) {
      localStorage.setItem('id_token', data.body.id_token);
      localStorage.setItem('user', (0, _stringify2.default)(data.body.user));

      var user = JSON.parse(localStorage.user);
      _store2.default.dispatch('setUser', user);
      _store2.default.dispatch('authenticateUser');
      if (redirect) {
        context.$router.push({ name: 'User', params: { id: user._id } });
      }
    }, function (err) {
      context.error = err;
      console.log(err);
    });
  },
  signup: function signup(context, creds, redirect) {
    context.$http.post(SIGNUP_URL, creds, function (data) {
      localStorage.setItem('id_token', data.id_token);

      if (redirect) {
        context.$router.go(redirect);
      }
    }).error(function (err) {
      context.error = err;
    });
  },
  logout: function logout(context) {
    localStorage.removeItem('id_token');
    localStorage.removeItem('user');
    _store2.default.dispatch('deauthenticateUser');
    context.$router.push({ name: 'Home' });
  },
  getAuthHeader: function getAuthHeader() {
    return 'JWT ' + localStorage.getItem('id_token');
  },
  requireTeacher: function requireTeacher(to, from, next) {
    console.log('requireTeacher executed!');
    console.log(_store2.default.getters.user.authenticated);
    if (!_store2.default.getters.user.authenticated) {
      next({ path: '/login' });
    } else if (_store2.default.getters.user.authenticated && _store2.default.getters.user.userRoles.indexOf("teacher") != -1) {
      next();
    }
  },
  requireAdmin: function requireAdmin(to, from, next) {
    console.log('requireAdmin executed!');
    console.log(_store2.default.getters.user.authenticated);
    if (!_store2.default.getters.user.authenticated) {
      next({ path: '/login' });
    } else if (_store2.default.getters.user.authenticated && _store2.default.getters.user.userRoles.indexOf("admin") != -1) {
      next();
    }
  },
  requireAuth: function requireAuth(to, from, next) {
    console.log('requireAuth executed!');
    console.log(_store2.default.getters.user.authenticated);
    console.log(to);
    if (!_store2.default.getters.user.authenticated) {
      next({ path: '/login' });
    } else {
      next(to);
    }
  }
};

//# sourceMappingURL=auth-compiled.js.map