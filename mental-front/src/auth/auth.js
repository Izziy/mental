import store from '../store/store'

// URL and endpoint constants
const API_URL = '/api';
const LOGIN_URL = API_URL + '/login';
const SIGNUP_URL = API_URL + '/signup';

export default {

  // Send a request to the login URL and save the returned JWT
  login(context, creds, redirect) {
    context.$http.post(LOGIN_URL, creds).then((data) => {
      localStorage.setItem('id_token', data.body.id_token);
      localStorage.setItem('user', JSON.stringify(data.body.user));

      let user = JSON.parse(localStorage.user);
      store.dispatch('setUser',user);
      store.dispatch('authenticateUser');
      if(redirect) {
        context.$router.push({name: 'User',params: {id: user._id}});
      }

    }, (err) => {
      context.error = err;
      console.log(err);
    });
  },

  signup(context, creds, redirect) {
    context.$http.post(SIGNUP_URL, creds, (data) => {
      localStorage.setItem('id_token', data.id_token);

      if(redirect) {
        context.$router.go(redirect)
      }

    }).error((err) => {
      context.error = err
    })
  },

  // To log out, we just need to remove the token
  logout(context) {
    localStorage.removeItem('id_token');
    localStorage.removeItem('user');
    store.dispatch('deauthenticateUser');
    context.$router.push({name: 'Home'});
  },

  /*checkAuth() {
    if (store.getters.user.authenticated != null) {
      let jwt = localStorage.getItem('id_token');
      let user = JSON.parse(localStorage.getItem('user'));
      console.log(jwt);
      console.log(user);
      if(jwt && user) {
        store.dispatch('setUser',{user});
        store.dispatch('authenticateUser');
        //store.actions.setUser(user);
        //store.actions.authenticateUser();
      } else {
        store.dispatch('deauthenticateUser');
        context.$router.push('login');
        //store.actions.deauthenticateUser();
      }
    } else {
      context.$router.push('login');
      console.log(true);
    }
  },*/

  // The object to be passed as a header for authenticated requests
  getAuthHeader() {
    return 'JWT ' + localStorage.getItem('id_token')
  },

  requireTeacher (to, from, next) {
    console.log('requireTeacher executed!');
    console.log(store.getters.user.authenticated);
    if (!store.getters.user.authenticated) {
      next({ path: '/login' });
    } else if (store.getters.user.authenticated && store.getters.user.userRoles.indexOf("teacher") != -1){
      next();
    }
  },
  requireAdmin (to, from, next) {
    console.log('requireAdmin executed!');
    console.log(store.getters.user.authenticated);
    if (!store.getters.user.authenticated) {
      next({ path: '/login' });
    } else if (store.getters.user.authenticated && store.getters.user.userRoles.indexOf("admin") != -1){
      next();
    }
  },
  requireAuth (to, from, next) {
    console.log('requireAuth executed!');
    console.log(store.getters.user.authenticated);
    console.log(to);
    if (!store.getters.user.authenticated) {
      next({ path: '/login' });
    } else {
      next(to);
    }
  }
}
