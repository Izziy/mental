import Vue from 'vue';

export const login = ({commit}) => {
  Vue.http.post('/login')
    .then(response => response.json())
    .then(data => {
      if(data) {
        const stocks = data.stocks;
        const funds = data.funds;
        const stockPortfolio = data.stockPortfolio;

        const portfolio = {
          stockPortfolio,
          funds
        };

        commit('SET_STOCKS', stocks);
        commit('SET_PORTFOLIO', portfolio);
      }
    })
};
