import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import game from './modules/game';
import * as actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    game
  },
  actions,
  strict: true
});
