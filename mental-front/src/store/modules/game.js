import mental from '../../mentalFunctions';

const state = {
  game: {
/*    options: {
      passedBone: 9,
      brother: 4,
      maxNumber: 9,
      maxCount: 5,
      delay: 500,
      doubleMode: false
    },
    status: {
      countdown: 3,
      counter: 0,
      result: {
        result1: 0,
        result2: 0,
        resultSet1: [],
        resultSet2: []
      },
      nextNumber1: 0,
      nextNumber2: 0
    },
    answer1: '',
    answer2: '',
    message: '',
    gameStarted: false,
    gameFinished: false,
    isAnswered: false,
    isCorrect: false*/
  },
  /*numberMap: {
    'simple': {
      '1': {
        '0': [0,1],
        '1': [-1,0,1],
        '2': [-1,0,1],
        '3': [-1,0,1],
        '4': [-1,0],
        '5': [0,1],
        '6': [-1,0,1],
        '7': [-1,0,1],
        '8': [-1,0,1],
        '9': [-1,0]
      },
      '2': {
        '0': [0,1,2],
        '1': [-1,0,1,2],
        '2': [-2,-1,0,1,2],
        '3': [-2,-1,0,1],
        '4': [-2,-1,0],
        '5': [0,1,2],
        '6': [-1,0,1,2],
        '7': [-2,-1,0,1,2],
        '8': [-2,-1,0,1],
        '9': [-2,-1,0]
      },
      '3': {
        '0': [0,1,2,3],
        '1': [-1,0,1,2,3],
        '2': [-2,-1,0,1,2],
        '3': [-3,-2,-1,0,1],
        '4': [-3,-2,-1,0],
        '5': [0,1,2,3],
        '6': [-1,0,1,2,3],
        '7': [-2,-1,0,1,2],
        '8': [-3,-2,-1,0,1],
        '9': [-3,-2,-1,0]
      },
      '4': {
        '0': [0,1,2,3,4],
        '1': [-1,0,1,2,3],
        '2': [-2,-1,0,1,2],
        '3': [-3,-2,-1,0,1],
        '4': [-4,-3,-2,-1,0],
        '5': [0,1,2,3,4],
        '6': [-1,0,1,2,3],
        '7': [-2,-1,0,1,2],
        '8': [-3,-2,-1,0,1],
        '9': [-4,-3,-2,-1,0]
      },
      '5': {
        '0': [0,1,2,3,4,5],
        '1': [-1,0,1,2,3,5],
        '2': [-2,-1,0,1,2,5],
        '3': [-3,-2,-1,0,1,5],
        '4': [-4,-3,-2,-1,0,5],
        '5': [-5,0,1,2,3,4],
        '6': [-5,-1,0,1,2,3],
        '7': [-5,-2,-1,0,1,2],
        '8': [-5,-3,-2,-1,0,1],
        '9': [-5,-4,-3,-2,-1,0]
      },
      '6': {
        '0': [0,1,2,3,4,5,6],
        '1': [-1,0,1,2,3,5,6],
        '2': [-2,-1,0,1,2,5,6],
        '3': [-3,-2,-1,0,1,5,6],
        '4': [-4,-3,-2,-1,0,5],
        '5': [-5,0,1,2,3,4],
        '6': [-6,-5,-1,0,1,2,3],
        '7': [-6,-5,-2,-1,0,1,2],
        '8': [-6,-5,-3,-2,-1,0,1],
        '9': [-6,-5,-4,-3,-2,-1,0]
      },
      '7': {
        '0': [0,1,2,3,4,5,6,7],
        '1': [-1,0,1,2,3,5,6,7],
        '2': [-2,-1,0,1,2,5,6,7],
        '3': [-3,-2,-1,0,1,5,6],
        '4': [-4,-3,-2,-1,0,5],
        '5': [-5,0,1,2,3,4],
        '6': [-6,-5,-1,0,1,2,3],
        '7': [-7,-6,-5,-2,-1,0,1,2],
        '8': [-7,-6,-5,-3,-2,-1,0,1],
        '9': [-7,-6,-5,-4,-3,-2,-1,0]
      },
      '8': {
        '0': [0,1,2,3,4,5,6,7,8],
        '1': [-1,0,1,2,3,5,6,7,8],
        '2': [-2,-1,0,1,2,5,6,7],
        '3': [-3,-2,-1,0,1,5,6],
        '4': [-4,-3,-2,-1,0,5],
        '5': [-5,0,1,2,3,4],
        '6': [-6,-5,-1,0,1,2,3],
        '7': [-7,-6,-5,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-3,-2,-1,0,1],
        '9': [-8,-7,-6,-5,-4,-3,-2,-1,0]
      },
      '9': {
        '0': [0,1,2,3,4,5,6,7,8,9],
        '1': [-1,0,1,2,3,5,6,7,8],
        '2': [-2,-1,0,1,2,5,6,7],
        '3': [-3,-2,-1,0,1,5,6],
        '4': [-4,-3,-2,-1,0,5],
        '5': [-5,0,1,2,3,4],
        '6': [-6,-5,-1,0,1,2,3],
        '7': [-7,-6,-5,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-3,-2,-1,0,1],
        '9': [-9,-8,-7,-6,-5,-4,-3,-2,-1,0]
      }
    },
    'brothers': {
      '1': {
        '0': [0,1,2,3,4,5,6,7,8,9],
        '1': [-1,0,1,2,3,4,5,6,7,8],
        '2': [-2,-1,0,1,2,3,4,5,6,7],
        '3': [-3,-2,-1,0,1,2,3,4,5,6],
        '4': [-4,-3,-2,-1,0,1,2,3,4,5],
        '5': [-5,-4,-3,-2,-1,0,1,2,3,4],
        '6': [-6,-5,-4,-3,-2,-1,0,1,2,3],
        '7': [-7,-6,-5,-4,-3,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-4,-3,-2,-1,0,1],
        '9': [-9,-8,-7,-6,-5,-4,-3,-2,-1,0]
      },
      '2': {
        '0': [0,1,2,3,4,5,6,7,8,9],
        '1': [-1,0,1,2,3,4,5,6,7,8],
        '2': [-2,-1,0,1,2,3,4,5,6,7],
        '3': [-3,-2,-1,0,1,2,3,4,5,6],
        '4': [-4,-3,-2,-1,0,2,3,4,5],
        '5': [-5,-4,-3,-2,0,1,2,3,4],
        '6': [-6,-5,-4,-3,-2,-1,0,1,2,3],
        '7': [-7,-6,-5,-4,-3,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-4,-3,-2,-1,0,1],
        '9': [-9,-8,-7,-6,-5,-4,-3,-2,-1,0]
      },
      '3': {
        '0': [0,1,2,3,4,5,6,7,8,9],
        '1': [-1,0,1,2,3,4,5,6,7,8],
        '2': [-2,-1,0,1,2,3,4,5,6,7],
        '3': [-3,-2,-1,0,1,3,4,5,6],
        '4': [-4,-3,-2,-1,0,3,4,5],
        '5': [-5,-4,-3,0,1,2,3,4],
        '6': [-6,-5,-4,-3,-1,0,1,2,3],
        '7': [-7,-6,-5,-4,-3,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-4,-3,-2,-1,0,1],
        '9': [-9,-8,-7,-6,-5,-4,-3,-2,-1,0]
      },
      '4': {
        '0': [0,1,2,3,4,5,6,7,8,9],
        '1': [-1,0,1,2,3,4,5,6,7,8],
        '2': [-2,-1,0,1,2,4,5,6,7],
        '3': [-3,-2,-1,0,1,4,5,6],
        '4': [-4,-3,-2,-1,0,4,5],
        '5': [-5,-4,0,1,2,3,4],
        '6': [-6,-5,-4,-1,0,1,2,3],
        '7': [-7,-6,-5,-4,-2,-1,0,1,2],
        '8': [-8,-7,-6,-5,-4,-3,-2,-1,0,1],
        '9': [-9,-8,-7,-6,-5,-4,-3,-2,-1,0]
      }
    },
    'friends': {

    },
    'anzan': {

    }
  },*/
  gameList: ['Простой','Братья','Друзья','Анзан']
};

const getters = {
  game: state => {
    return state.game;
  },
  gameList: state => {
    return state.gameList;
  },
  typeSys: state => {
    return state.game.typeSys;
  },
  type: state => {
    return state.game.type;
  },
  numberMap: state => {
    return state.numberMap;
  }
};

const mutations = {
  'SELECT_GAME' (state, game) {
    state.game.type = game;
    if (game == 'Простой') {
      state.game.typeSys = 'simple';
    } else if (game == 'Братья'){
      state.game.typeSys = 'brothers';
    } else if (game == 'Друзья'){
      state.game.typeSys = 'friends';
    } else if (game == 'Анзан'){
      state.game.typeSys = 'anzan';
    }
  },
  'INIT_GAME' (state) {
    state.game = {
      type: null,
      typeSys: null,
      options: {
        passedBone: 9,   // изученное число, используется в простом
        brother: 4,      // состав числа 4, используется в братьях
        friend: 9,      // состав числа 10, используется в друзьях
        maxNumber: 9,
        maxCount: 5,
        delay: 3000,
        doubleMode: false
      },
      status: {
        countdown: 3,
        counter: 0,
        result: {
          result1: 0,
          result2: 0,
          resultSet1: [],
          resultSet2: []
        },
        nextNumber1: 0,
        nextNumber2: 0
      },
      message: '',
      gameStarted: false,
      gameFinished: false,
      isAnswered: false,
      isCorrect: false
    };
  },
  'START_GAME' (state) {
    state.game.gameStarted = true;
    state.game.gameFinished = false;
    state.game.isAnswered = false;
    state.game.isCorrect = false;
    state.game.answer1 = '';
    state.game.answer2 = '';
    state.game.status = {
      countdown: 3,
      counter: 0,
      result: {
        result1: 0,
        result2: 0,
        resultSet1: [],
        resultSet2: [],
      },
      nextNumber1: 0,
      nextNumber2: 0
    };

    let maxNumber = state.game.options.maxNumber;
    let maxCount = state.game.options.maxCount;
    let doubleMode = state.game.options.doubleMode;
    let type = state.game.typeSys;

    let passedBone = state.game.options.passedBone;
    let brother = state.game.options.brother;
    let friend = state.game.options.friend;
    let modifier = null;
    if (type == 'simple') {
      modifier = passedBone;
    } else if (type == 'brothers') {
      modifier = brother;
    } else if (type == 'friend') {
      modifier = friend;
    }

    let res = mental.generateSimpleSet(maxNumber, maxCount, type, modifier);
    state.game.status.result.result1 = res.result;
    state.game.status.result.resultSet1 = res.resultSet;
    if (doubleMode) {
      let res2 = mental.generateSimpleSet(maxNumber, maxCount, type, modifier);
      state.game.status.result.result2 = res2.result;
      state.game.status.result.resultSet2 = res2.resultSet;
    }
  },
  'CHECK_ANSWER' (state, answers) {
    state.game.isAnswered = true;
    let answer1 = parseInt(answers.answer1);
    let answer2 = parseInt(answers.answer2);
    if (!state.game.options.doubleMode) {
      if (answer1 == state.game.status.result.result1) {
        state.game.message = 'Правильно!';
        state.game.isCorrect = true;
      } else {
        state.game.message = 'Неправильно!';
        state.game.isCorrect = false;
      }
    } else {
      if (answer1 == state.game.status.result.result1 && answer2 == state.game.status.result.result2) {
        state.game.message = 'Правильно!';
        state.game.isCorrect = true;
      } else if (answer1 != state.game.status.result.result1 && answer2 != state.game.status.result.result2) {
        state.game.message = 'Оба ответа неверны!';
        state.game.isCorrect = false;
      } else {
        state.game.message = '1 ответ не верен!';
        state.game.isCorrect = false;
      }
    }
  },
  'UPDATE_PASSEDBONE' (state, value) {
    state.game.options.passedBone = parseInt(value)
  },
  'UPDATE_BROTHER' (state, value) {
    state.game.options.brother = parseInt(value)
  },
  'UPDATE_FRIEND' (state, value) {
    state.game.options.friend = parseInt(value)
  },
  'UPDATE_MAXNUMBER' (state, value) {
    state.game.options.maxNumber = parseInt(value)
  },
  'UPDATE_MAXCOUNT' (state, value) {
    state.game.options.maxCount = parseInt(value)
  },
  'UPDATE_DELAY' (state, value) {
    state.game.options.delay = parseInt(value)
  },
  'UPDATE_DOUBLEMODE' (state, value) {
    state.game.options.doubleMode = value
  },
  'DECREMENT_COUNTDOWN' (state) {
    state.game.status.countdown -= 1;
  },
  'INCREMENT_COUNTER' (state) {
    state.game.status.counter++;
  },
  'NEXT_NUMBER' (state) {
    state.game.status.nextNumber1 = state.game.status.result.resultSet1[state.game.status.counter];
    state.game.status.nextNumber2 = state.game.status.result.resultSet2[state.game.status.counter];
  },
  'FINISH_GAME' (state) {
    state.game.gameFinished = true;
  },
};

const actions = {
  selectGame: ({commit}, game) => {
    commit('SELECT_GAME', game);
  },
  initGame: ({commit}) => {

    commit('INIT_GAME');
  },
  startGame: ({commit}) => {
    commit('START_GAME');
    let startInterval = setInterval(() => {

      commit('DECREMENT_COUNTDOWN');
      if (state.game.status.countdown == 0) {

        clearInterval(startInterval);
        commit('NEXT_NUMBER');

        let taskInterval = setInterval(()=> {

          commit('INCREMENT_COUNTER');
          commit('NEXT_NUMBER');

          if (state.game.status.counter == state.game.status.result.resultSet1.length) {
            clearInterval(taskInterval);
            commit('FINISH_GAME');
          }

        }, state.game.options.delay);

      }
    },1000);
  },
  checkAnswer: ({commit}, answers) => {
    commit('CHECK_ANSWER', answers);
  },
  showAnswer: () => {

  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
