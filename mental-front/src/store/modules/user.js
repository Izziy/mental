import Vue from 'vue'

const state = {
  user: {}
};

const getters = {
  user: state => {
    return state.user;
  }
};

const mutations = {
  'SET_USER' (state, user) {
    state.user = user;
  },
  'AUTHENTICATE_USER' (state) {
    state.user.authenticated = true;
  },
  'DEAUTHENTICATE_USER' (state) {
    state.user = {};
  },
  'ADD_PARENT_INFO' (state, parent) {
    state.user.parents.push(parent);
    console.log(parent);
    let payload = {
      userId: state.user._id,
      parent: parent
    };
    Vue.http.post('/api/addParentInfo', payload).then((data) => {
      console.log('Success', data);
    }, (error) => {
      console.log('Error', error);
    });
  },
};

const actions = {
  setUser: ({commit}, user) => {
    commit('SET_USER', user);
  },
  authenticateUser: ({commit}) => {
    commit('AUTHENTICATE_USER');
  },
  deauthenticateUser: ({commit}) => {
    commit('DEAUTHENTICATE_USER');
  },
  addParentInfo: ({commit}, parent) => {
    commit('ADD_PARENT_INFO', parent);
  },
};

export default {
  state,
  getters,
  mutations,
  actions
}
