"use strict";

const express      = require('express');
const path         = require('path');
const app          = express();

const history      = require('connect-history-api-fallback');
//let passport     = require('passport');
const flash        = require('connect-flash');
const morgan       = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser   = require("body-parser");
//let session      = require('express-session');
const auth = require("./app/config/auth")();


//require('./app/config/passport')(passport); // pass passport for configuration

const staticFileMiddleware = express.static(path.join(__dirname + '/public'));
// set up our express application
app.use(staticFileMiddleware);
app.use(history());
app.use(staticFileMiddleware);
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
//app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
/*app.use(session({
    secret: 'itstimetomakesomementaltraining', // session secret
    resave: true,
    saveUninitialized: true
}));*/
app.use(auth.initialize());
//app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


// routes ======================================================================
require('./app/routes/index.js')(app); // load our routes and pass in our app and fully configured passport

module.exports = app;