# Mental arithmetic

Code for the Mental arithmetic project

## Instructions
1. Clone the repo: `git clone git@gitlab.com:Izziy/mental.git`
2. Install packages: `npm install`
2.1. cd mental-front and 'npm install' one more time
3. Change out the database configuration in config/database.js (optinal)
4. Change out auth keys in config/auth.js (optinal)
5. If local start mongo server by `mongod`
6. Launch: `bin\www`
7. cd mental-front and launch front-end for dev mode 'npm run dev'
7. Visit in your browser at: `http://localhost:8080`

## GIT
1. Check status `git status`
2. Check changes `git diff`
3. Add files `git add .`
4. Commit `git commit -m "Commit message"`
5. Push `git push -u origin master`
6. Pull `git pull origin master`

## NPM
1. `npm shrinkwrap` then added a new none-dev-dependency to fix the version of it.
2. `npm run test` to run tests from `./tests/tests.js`.
3. `npm run watch` to start transpile watch. This command will read files under `./public` and generate a single file under `./public/dist/bundle.js` which should be included by index.html.
Used only for Angular front-end files. Uses `babel` to transpile ESMA6 into ECMA5.

## Heroku
1. Install heroku through `https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up`
2. `heroku login` for initial
3. `heroku create` for initial
4. `git push heroku master` after commit to git
5. `heroku open` to go to url
6. `heroku logs --tail` to check logs


### Optional info
Used Passport to authenticate users locally, with Facebook, Twitter, and Google (optional).

### The Tutorials in Scotch.io for auth

- [Getting Started and Local Authentication](http://scotch.io/tutorials/javascript/easy-node-authentication-setup-and-local)
- [Facebook](http://scotch.io/tutorials/javascript/easy-node-authentication-facebook)
- [Twitter](http://scotch.io/tutorials/javascript/easy-node-authentication-twitter)
- [Google](http://scotch.io/tutorials/javascript/easy-node-authentication-google)
- [Linking All Accounts Together](http://scotch.io/tutorials/javascript/easy-node-authentication-linking-all-accounts-together)