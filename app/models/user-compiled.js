'use strict';

// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    email: String,
    password: String,
    dateCreated: { type: Date, required: true, default: Date.now },
    username: String,
    firstName: String,
    lastName: String,
    gender: String,
    birthDate: String,
    country: String,
    city: String,
    phone: String,
    userRoles: {
        type: Array,
        default: ['user']
    },
    parents: [],
    courses: {
        requests: [],
        studying: [],
        teaching: []
    },
    teacherCourses: [],
    teachers: {
        requests: [],
        current: []
    },
    students: {
        requests: [],
        current: []
    },
    mental: {
        gameNumber: {
            simple: 0,
            brothers: 0,
            friends: 0,
            anzan: 0
        },
        gameWins: {
            simple: 0,
            brothers: 0,
            friends: 0,
            anzan: 0
        },
        homeWorks: []
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    }
});

// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);

//# sourceMappingURL=user-compiled.js.map