// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var teacherCourseSchema = mongoose.Schema({
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Course'
    },
    rating: 0,
    feedbacks: [],
    studentRequests: [],
    students: []
});

// create the model for users and expose it to our app
module.exports = mongoose.model('TeacherCourse', teacherCourseSchema);
