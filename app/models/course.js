var mongoose = require('mongoose');

var courseSchema = mongoose.Schema({
    _id: { type: String, required: true},
    title: String,
    description: String,
    dateCreated: { type: Date, required: true, default: Date.now },
    rating: {
        type: Number,
        default: 0
    },
    isSelfStudy: Boolean,
    studyRequests: [],
    teachRequests: [],
    students: [],
    teachers: []
});

module.exports = mongoose.model('Course', courseSchema);
