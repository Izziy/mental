// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
let teacherCourseSchema  = require('./teacherCourse').schema;

// define the schema for our user model
var userSchema = mongoose.Schema({
    email        : String,
    password     : String,
    dateCreated  : { type: Date, required: true, default: Date.now },
    verified     : {
        email: { type: Boolean, default: false },
        phone: { type: Boolean, default: false }
    },
    username     : String,
    firstName    : String,
    lastName     : String,
    gender       : String,
    birthDate    : String,
    country      : String,
    city         : String,
    phone        : String,
    userRoles    : {
        type: Array,
        default: ['user']
    },
    parents      : [],
    courses      : {
        requests: [],
        studying: [],
        teaching: [teacherCourseSchema]
    },
    teacherCourses: [],
    teachers     : {
        requests: [],
        current: []
    },
    students     : {
        requests: [],
        current: []
    },
    mental: {
        gameNumber: {
            simple: {
                type: Number,
                default: 0
            },
            brothers: {
                type: Number,
                default: 0
            },
            friends: {
                type: Number,
                default: 0
            },
            anzan: {
                type: Number,
                default: 0
            }
        },
        gameWins: {
            simple: {
                type: Number,
                default: 0
            },
            brothers: {
                type: Number,
                default: 0
            },
            friends: {
                type: Number,
                default: 0
            },
            anzan: {
                type: Number,
                default: 0
            }
        },
        homeWorks: []
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }
});

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
