"use strict";

let passport = require('passport');
let jwt = require("jwt-simple");

let User  = require('../models/user');
let cfg = require("../config/config.js");
let auth = require("../config/auth")();
let common = require('../common');

module.exports = function (app) {

    app.get('/api/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/api/login', function(req, res) {
        //res.send({ message: req.flash('loginMessage')});
    });

/*    app.post('/api/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));*/

    app.post('/api/login', function(req, res) {
        // console.log(req.headers);
        if (req.body.email && req.body.password) {
            let email = req.body.email.toLowerCase();
            let password = req.body.password;
            process.nextTick(function() {
                User.findOne({ 'email' : email }, function(err, user) {
                    if (err) {
                        return res.status(401).send(err);
                    }
                    if (!user) {
                        return res.status(401).send("Пользователь не найден.");
                    }
                    if (!user.validPassword(password)) {
                        return res.status(401).send("Неверный пароль.");
                    }
                    else {
                        let payload = {
                            id: user._id
                        };
                        let id_token = jwt.encode(payload, cfg.jwtSecret);
                        //console.log(user);
                        return res.json({
                            user: user,
                            id_token: id_token
                        });
                    }
                });
            });
        } else {
            return res.sendStatus(401);
        }
    });

    app.get('/api/signup', function(req, res) {
        res.send({ message: req.flash('signupMessage')});
    });

    app.post('/api/signup', function (req, res) {
        console.log(req.body);
        if (req.body.email && req.body.password) {
            let email = req.body.email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
            let password = req.body.password;
            let userRole = req.body.userRole;
            // let username = req.body.username;
            let firstName = req.body.firstName;
            let lastName = req.body.lastName;
            // let gender = req.body.gender;
            // let birthDate = req.body.birthDate;
            // let country = req.body.country;
            // let city = req.body.city;
            // let phone = req.body.phone;

            // asynchronous
            process.nextTick(function() {
                // if the user is not already logged in:
                if (!req.body.user) {
                    User.findOne({ 'email' :  email }, function(err, user) {
                        // if there are any errors, return the error
                        if (err)
                            return res.status(401).send(err);

                        // check to see if theres already a user with that email
                        if (user) {
                            return res.status(409).send("Этот email уже занят.");
                        } else {

                            // create the user
                            let newUser            = new User();

                            newUser.email     = email;
                            newUser.password  = newUser.generateHash(password);

                            if (userRole == 'teacher') {
                                newUser.userRoles.push(userRole);
                            }
                            // newUser.username  = username;
                            newUser.firstName = firstName;
                            newUser.lastName  = lastName;
                            // newUser.gender    = gender;
                            // newUser.birthDate = birthDate;
                            // newUser.country   = country;
                            // newUser.city      = city;
                            // newUser.phone     = phone;


                            newUser.save(function(err) {
                                if (err) {
                                    return res.status(401).send(err);
                                }
                                return res.status(200).send("Аккаунт создан.");
                            });
                        }

                    });
                    // if the user is logged in but has no local account...
                } else if ( !req.user.email ) {
                    // ...presumably they're trying to connect a local account
                    // BUT let's check if the email used to connect a local account is being used by another user
                    User.findOne({ 'email' :  email }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {
                            return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                            // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                        } else {
                            var user = req.user;
                            user.email = email;
                            user.password = user.generateHash(password);
                            user.save(function (err) {
                                if (err)
                                    return done(err);

                                return done(null,user);
                            });
                        }
                    });
                } else {
                    // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                    return done(null, req.user);
                }

            });
        } else {
            return res.sendStatus(401);
        }
    });
/*
    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/api/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/api/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/api/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));

    // handle the callback after twitter has authenticated the user
    app.get('/api/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));


    // google ---------------------------------

    // send to google to do the authentication
    app.get('/api/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/api/auth/google/callback',
        passport.authenticate('google', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

    // locally --------------------------------
    app.get('/api/connect/local', function(req, res) {
        res.render('connect-local.ejs', { message: req.flash('loginMessage') });
    });
    app.post('/api/connect/local', passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/api/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

    // handle the callback after facebook has authorized the user
    app.get('/api/connect/facebook/callback',
        passport.authorize('facebook', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/api/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

    // handle the callback after twitter has authorized the user
    app.get('/api/connect/twitter/callback',
        passport.authorize('twitter', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));


    // google ---------------------------------

    // send to google to do the authentication
    app.get('/api/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

    // the callback after google has authorized the user
    app.get('/api/connect/google/callback',
        passport.authorize('google', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

    // local -----------------------------------
    app.get('/api/unlink/local', common.isLoggedIn, function(req, res) {
        var user            = req.user;
        user.local.email    = undefined;
        user.local.password = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // facebook -------------------------------
    app.get('/api/unlink/facebook', common.isLoggedIn, function(req, res) {
        var user            = req.user;
        user.facebook.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // twitter --------------------------------
    app.get('/api/unlink/twitter', common.isLoggedIn, function(req, res) {
        var user           = req.user;
        user.twitter.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

    // google ---------------------------------
    app.get('/api/unlink/google', common.isLoggedIn, function(req, res) {
        var user          = req.user;
        user.google.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });*/
};