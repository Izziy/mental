const User  = require('../models/user');
const Course  = require('../models/course');
const common = require('../common');
const auth = require("../config/auth")();
const url = require('url');
const async = require('async');
const jwt = require("jwt-simple");
const cfg = require("../config/config.js");

module.exports = function (app) {

    app.post('/api/postTeachCourseRequest', auth.authenticate(), function(req, res) {
        let userId = jwt.decode(req.headers.authorization.slice(4), cfg.jwtSecret).id; //пиздец какой костыль, но нет времени разбираться с авторизацией
        Course.findById(req.body.courseId, 'teachRequests', function (err, course) {
            if (err) return console.log(err);
            let isRepeated = false;
            let message = '';
            async.each(course.teachRequests, function(request, callback) {
                if (request.userId == userId) {
                    message = 'вай вай вай уже есть такой запрос';
                    isRepeated = true;
                    res.status(422).send('вай вай вай уже есть такой запрос');
                }
            }, function() {
                let dateRequested = new Date();
                let request = {
                    dateRequested: dateRequested,
                    userId: userId,
                    accepted: false,
                    acceptedBy: null
                };
                course.teachRequests.push(request);
                course.save(function (err, updatedCourse) {
                    if (err) return console.log(err);

                    User.findById(userId, 'teachRequests', function (err, user) {
                        if (err) return console.log(err);
                        console.log(user);
                    });

                    res.json(updatedCourse);
                });
            });

        });
    });
};