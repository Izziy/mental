module.exports = function (app) {
    require("./main")(app);
    require("./admin")(app);
    require("./auth")(app);
    require("./teacher")(app);
    // add new lines for each other module, or use an array with a forEach
};