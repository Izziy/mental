const User  = require('../models/user');
const Course  = require('../models/course');
const common = require('../common');
const auth = require("../config/auth")();
const url = require('url');
var async = require('async')

module.exports = function (app) {

    app.post('/api/changeAdminStatus', function(req, res) {
        User.findById(req.body.id, function (err, user) {
            if (err) return console.log(err);
            console.log(user.userRoles);
            if (user.userRoles.indexOf('admin') == -1) {
                user.userRoles.push('admin');
            } else if (user.userRoles.indexOf('admin') != -1) {
                user.userRoles.splice(user.userRoles.indexOf('admin'), 1);
            }
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser);
            });
        });
    });

    app.post('/api/changeTeacherStatus', function(req, res) {
        User.findById(req.body.id, function (err, user) {
            if (err) return console.log(err);
            console.log(user.userRoles);
            if (user.userRoles.indexOf('teacher') == -1) {
                user.userRoles.push('teacher');
            } else if (user.userRoles.indexOf('teacher') != -1) {
                user.userRoles.splice(user.userRoles.indexOf('teacher'), 1);
            }
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser);
            });
        });
    });

    app.post('/api/createCourse', function(req, res) {
        let newCourse = new Course(req.body);
        newCourse.save(function(err) {
            if (err) {
                return res.status(401).send(err);
            }
            return res.status(200).send("Course created.");
        });
    });

    app.get('/api/getAdminTeachRequests', function(req, res) {
        Course.find({teachRequests: { $gt: [] }}, 'title teachRequests', function(err, courses) {
            if (err) return console.log(err);
            let requests = [];
            async.each(courses, function(course, callback) {
                async.each(course.teachRequests, function(userId, next) {
                    User.findById(userId, 'email firstName lastName', function (err, user) {
                        //console.log(user);
                        let courseRequest = {
                            courseId: course._id,
                            courseTitle: course.title,
                            teacherId: user._id,
                            email: user.email,
                            firstName: user.firstName,
                            lastName: user.lastName,
                        };
                        requests.push(courseRequest);
                        next();
                    });
                }, function() {
                    callback();
                });
            }, function() {
                //console.log(requests);
                res.send(requests);
            });
        });
    });

    app.post('/api/admin/acceptTeachRequest', function(req, res) {
        //console.log(req.body);
        let request = req.body;
        Course.findById(request.courseId, 'teachRequests teachers', function (err, course) {
            if (err) return console.log(err);
            let teacher = {
                teacherId: request.teacherId,
                rating: 0,
                feedbacks: []
            };
            course.teachers.push(teacher);
            course.teachRequests.splice(course.teachRequests.indexOf(request.teacherId),1);
            course.save(function (err, updatedCourse) {
                if (err) return console.log(err);
                res.send('Request accepted');
            });
        });
        // User.findById(req.body.id, function (err, user) {
        //     if (err) return console.log(err);
        //     console.log(user.userRoles);
        //     if (user.userRoles.indexOf('teacher') == -1) {
        //         user.userRoles.push('teacher');
        //     } else if (user.userRoles.indexOf('teacher') != -1) {
        //         user.userRoles.splice(user.userRoles.indexOf('teacher'), 1);
        //     }
        //     user.save(function (err, updatedUser) {
        //         if (err) return console.log(err);
        //         res.send(updatedUser);
        //     });
        // });
    });

    app.post('/api/admin/denyTeachRequest', function(req, res) {
        User.findById(req.body.id, function (err, user) {
            if (err) return console.log(err);
            console.log(user.userRoles);
            if (user.userRoles.indexOf('teacher') == -1) {
                user.userRoles.push('teacher');
            } else if (user.userRoles.indexOf('teacher') != -1) {
                user.userRoles.splice(user.userRoles.indexOf('teacher'), 1);
            }
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser);
            });
        });
    });

};