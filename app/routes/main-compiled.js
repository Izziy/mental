'use strict';

var User = require('../models/user');
var Course = require('../models/course');
var common = require('../common');
var auth = require("../config/auth")();
var url = require('url');
var async = require('async');

module.exports = function (app) {

    app.get("/", function (req, res) {
        res.render(path.join(__dirname + '/index.html'));
    });

    app.get('/api/profile', auth.authenticate(), function (req, res) {
        var user = req.user;
        res.json(user);
        console.log('Только если авторизовался!');
    });

    app.get('/api/adminpanel', common.isLoggedIn, common.isAllowed('admin'), function (req, res) {
        var user = req.user;
        res.json(user);
    });

    app.get('/api/getUsers', function (req, res) {
        User.find({}, 'email username firstName lastName birthDate phone userRoles', function (err, users) {
            if (err) return console.log(err);
            res.json(users);
        });
    });

    app.get('/api/getCourses', function (req, res) {
        Course.find({}, 'dateCreated title description rating', function (err, courses) {
            if (err) return console.log(err);
            res.json(courses);
        });
    });
    app.get('/api/getUserProfile', function (req, res) {
        var id = url.parse(req.url, true).query.id;
        User.findById(id, 'email username firstName lastName gender birthDate country city phone', function (err, user) {
            if (err) return console.log(err);
            res.json(user);
        });
    });
    app.get('/api/getCourseByID', function (req, res) {
        var id = url.parse(req.url, true).query.id;
        Course.findById(id, 'title description isSelfStudy', function (err, course) {
            if (err) return console.log(err);
            res.json(course);
        });
    });
    app.get('/api/getCourseTeachersByID', function (req, res) {
        var courseId = url.parse(req.url, true).query.id;
        // User.find({
        //     teacherCourses: courseId
        // }, 'lastName firstName', function (err, courseTeachers) {
        //     if (err) return console.log(err);
        //     res.json(courseTeachers);
        // });
        Course.findById(courseId, 'teachers', function (err, course) {
            if (err) return console.log(err);
            //console.log(course);
            res.send(course.teachers);
            //res.json(courseTeachers);
        });
    });
    app.get('/api/getTeacherCourses', function (req, res) {
        var userId = url.parse(req.url, true).query.id;
        Course.find({ teachRequests: userId }, 'title', function (err, courses) {
            if (err) return console.log(err);
            console.log(courses);
            res.send(courses);
        });

        // User.findById(userId, 'teacherCourses', function (err, user) {
        //     if (err) return console.log(err);
        //     Course.find({
        //         '_id': { $in: user.teacherCourses}
        //     }, function(err, courses){
        //         res.send(courses);
        //     });
        // });
    });
    app.post('/api/teachRequest', function (req, res) {
        // User.findById(req.body.userId, function (err, user) {
        //     if (err) return console.log(err);
        //     if (user.teacherCourses.indexOf(req.body.courseId) == -1) {
        //         user.teacherCourses.push(req.body.courseId);
        //         user.save(function (err, updatedUser) {
        //             if (err) return console.log(err);
        //             res.send(updatedUser);
        //         });
        //     } else {
        //         res.send('Course already teaching');
        //     }
        // });
        var payload = req.body;
        console.log(payload);
        Course.findById(payload.courseId, 'teachRequests', function (err, course) {
            if (err) return console.log(err);
            console.log(course);
            course.teachRequests.push(payload.userId);
            course.save(function (err, updatedCourse) {
                if (err) return console.log(err);
                res.send('Success');
            });
        });
    });
    app.put('/api/studyRequest', function (req, res) {
        var payload = req.body;
        console.log(payload);
        Course.findById(payload.courseId, 'requests', function (err, course) {
            if (err) return console.log(err);
            console.log(course);
            var request = {
                userId: payload.userId,
                teacherId: payload.teacherId,
                isSelfStudy: payload.isSelfStudy
            };
            course.studyRequests.push(request);

            course.save(function (err, updatedCourse) {
                if (err) return console.log(err);
                res.send('Success');
            });
        });
    });

    app.post('/api/addParentInfo', function (req, res) {
        var newParent = req.body.parent;
        console.log(newParent);
        User.findById(req.body.userId, function (err, user) {
            if (err) return console.log(err);
            user.parents.push(newParent);
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser);
            });
        });
    });

    app.get('/api/getTheFile', function (req, res) {
        var fileName = 'C:/Development/mental/app/routes/Wat_Metta_Buddha_Qualities.mp3';
        console.log(fileName);
        res.sendFile(fileName);
    });
};

//# sourceMappingURL=main-compiled.js.map