"use strict";

var passport = require('passport');
var jwt = require("jwt-simple");

var User = require('../models/user');
var cfg = require("../config/config.js");
var auth = require("../config/auth")();
var common = require('../common');

module.exports = function (app) {

    app.get('/api/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/api/login', function (req, res) {
        //res.send({ message: req.flash('loginMessage')});
    });

    /*    app.post('/api/login', passport.authenticate('local-login', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));*/

    app.post('/api/login', function (req, res) {
        console.log(req.headers);
        if (req.body.email && req.body.password) {
            (function () {
                var email = req.body.email.toLowerCase();
                var password = req.body.password;
                process.nextTick(function () {
                    User.findOne({ 'email': email }, function (err, user) {
                        if (err) {
                            return res.status(401).send(err);
                        }
                        if (!user) {
                            return res.status(401).send("No user found.");
                        }
                        if (!user.validPassword(password)) {
                            return res.status(401).send("Oops! Wrong password.");
                        } else {
                            var payload = {
                                id: user._id
                            };
                            var id_token = jwt.encode(payload, cfg.jwtSecret);
                            //console.log(user);
                            return res.json({
                                user: user,
                                id_token: id_token
                            });
                        }
                    });
                });
            })();
        } else {
            return res.sendStatus(401);
        }
    });

    app.get('/api/signup', function (req, res) {
        res.send({ message: req.flash('signupMessage') });
    });

    app.post('/api/signup', function (req, res) {

        if (req.body.email) {
            (function () {
                var email = req.body.email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
                var password = req.body.password;
                var username = req.body.username;
                var firstName = req.body.firstName;
                var lastName = req.body.lastName;
                var gender = req.body.gender;
                var birthDate = req.body.birthDate;
                var country = req.body.country;
                var city = req.body.city;
                var phone = req.body.phone;

                // asynchronous
                process.nextTick(function () {
                    // if the user is not already logged in:
                    if (!req.body.user) {
                        User.findOne({ 'email': email }, function (err, user) {
                            // if there are any errors, return the error
                            if (err) return res.status(401).send(err);

                            // check to see if theres already a user with that email
                            if (user) {
                                return res.status(409).send("That email is already taken.");
                            } else {

                                // create the user
                                var newUser = new User();

                                newUser.email = email;
                                newUser.password = newUser.generateHash(password);
                                newUser.username = username;
                                newUser.firstName = firstName;
                                newUser.lastName = lastName;
                                newUser.gender = gender;
                                newUser.birthDate = birthDate;
                                newUser.country = country;
                                newUser.city = city;
                                newUser.phone = phone;

                                newUser.save(function (err) {
                                    if (err) {
                                        return res.status(401).send(err);
                                    }
                                    return res.status(200).send("User created.");
                                });
                            }
                        });
                        // if the user is logged in but has no local account...
                    } else if (!req.user.email) {
                        // ...presumably they're trying to connect a local account
                        // BUT let's check if the email used to connect a local account is being used by another user
                        User.findOne({ 'email': email }, function (err, user) {
                            if (err) return done(err);

                            if (user) {
                                return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                                // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                            } else {
                                var user = req.user;
                                user.email = email;
                                user.password = user.generateHash(password);
                                user.save(function (err) {
                                    if (err) return done(err);

                                    return done(null, user);
                                });
                            }
                        });
                    } else {
                        // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                        return done(null, req.user);
                    }
                });
            })();
        }
    });
    /*
        // facebook -------------------------------
    
        // send to facebook to do the authentication
        app.get('/api/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
    
        // handle the callback after facebook has authenticated the user
        app.get('/api/auth/facebook/callback',
            passport.authenticate('facebook', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
        // twitter --------------------------------
    
        // send to twitter to do the authentication
        app.get('/api/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));
    
        // handle the callback after twitter has authenticated the user
        app.get('/api/auth/twitter/callback',
            passport.authenticate('twitter', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
    
        // google ---------------------------------
    
        // send to google to do the authentication
        app.get('/api/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    
        // the callback after google has authenticated the user
        app.get('/api/auth/google/callback',
            passport.authenticate('google', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
    // =============================================================================
    // AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
    // =============================================================================
    
        // locally --------------------------------
        app.get('/api/connect/local', function(req, res) {
            res.render('connect-local.ejs', { message: req.flash('loginMessage') });
        });
        app.post('/api/connect/local', passport.authenticate('local-signup', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
        // facebook -------------------------------
    
        // send to facebook to do the authentication
        app.get('/api/connect/facebook', passport.authorize('facebook', { scope : 'email' }));
    
        // handle the callback after facebook has authorized the user
        app.get('/api/connect/facebook/callback',
            passport.authorize('facebook', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
        // twitter --------------------------------
    
        // send to twitter to do the authentication
        app.get('/api/connect/twitter', passport.authorize('twitter', { scope : 'email' }));
    
        // handle the callback after twitter has authorized the user
        app.get('/api/connect/twitter/callback',
            passport.authorize('twitter', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
    
        // google ---------------------------------
    
        // send to google to do the authentication
        app.get('/api/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));
    
        // the callback after google has authorized the user
        app.get('/api/connect/google/callback',
            passport.authorize('google', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));
    
    // =============================================================================
    // UNLINK ACCOUNTS =============================================================
    // =============================================================================
    // used to unlink accounts. for social accounts, just remove the token
    // for local account, remove email and password
    // user account will stay active in case they want to reconnect in the future
    
        // local -----------------------------------
        app.get('/api/unlink/local', common.isLoggedIn, function(req, res) {
            var user            = req.user;
            user.local.email    = undefined;
            user.local.password = undefined;
            user.save(function(err) {
                res.redirect('/profile');
            });
        });
    
        // facebook -------------------------------
        app.get('/api/unlink/facebook', common.isLoggedIn, function(req, res) {
            var user            = req.user;
            user.facebook.token = undefined;
            user.save(function(err) {
                res.redirect('/profile');
            });
        });
    
        // twitter --------------------------------
        app.get('/api/unlink/twitter', common.isLoggedIn, function(req, res) {
            var user           = req.user;
            user.twitter.token = undefined;
            user.save(function(err) {
                res.redirect('/profile');
            });
        });
    
        // google ---------------------------------
        app.get('/api/unlink/google', common.isLoggedIn, function(req, res) {
            var user          = req.user;
            user.google.token = undefined;
            user.save(function(err) {
                res.redirect('/profile');
            });
        });*/
};

//# sourceMappingURL=auth-compiled.js.map