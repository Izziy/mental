const User  = require('../models/user');
const Course  = require('../models/course');
const common = require('../common');
const auth = require("../config/auth")();
const url = require('url');
const async = require('async');
const jwt = require("jwt-simple");
const cfg = require("../config/config.js");
const multer  = require('multer');
const path         = require('path');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname))
    }
});

let upload = multer({ storage: storage });

module.exports = function (app) {
    
    app.get("/", function (req, res) {
        res.render(path.join(__dirname + '/index.html'))
    });

    app.post('/api/uploads', upload.single('image'), (req, res) => {
        return res.json('success');
    });

    app.get('/api/initUserInfo', function(req, res) {
        console.log(req.headers.authorization);
        let id = jwt.decode(req.headers.authorization.slice(4), cfg.jwtSecret).id; //пиздец какой костыль, но нет времени разбираться с авторизацией
        User.findOne({ '_id' : id }, function(err, user) {
            Course.find({'_id': { $in: user.courses.studying}}, 'title', function(err, courses) {
                user.courses.studying = courses;
                res.json(user);
            });
        });

    });

    app.get('/api/profile', auth.authenticate(), function(req, res) {
        var user = req.user;
        res.json(user);
        console.log('Только если авторизовался!')
    });

    app.get('/api/adminpanel', common.isLoggedIn, common.isAllowed('admin'), function(req, res) {
        var user = req.user;
        res.json(user);
    });

    app.get('/api/getUsers', function(req, res) {
        User.find({'verified.personalInfo': true}, 'email username firstName lastName birthDate phone userRoles', function(err, users) {
            if (err) return console.log(err);
            res.json(users);
        });
    });


    app.get('/api/getCourses', function(req, res) {
        console.log('getCourses');
        Course.find({}, 'dateCreated title description rating', function(err, courses) {
            if (err) return console.log(err);
            res.json(courses);
        });
    });
    app.get('/api/getUserCourses', function(req, res) {
        console.log('getUserCourses');
        let userId = jwt.decode(req.headers.authorization.slice(4), cfg.jwtSecret).id;
        console.log(userId);
        User.findById(userId)
            .select('firstName lastName courses courses courses.studying') //хуета
            .exec(function(err, user) {
                if (err) return console.log(err);
                console.log(user);
            });
    });
    app.get('/api/getUserProfile', function(req, res) {
        let id = url.parse(req.url, true).query.id;
        User.findById(id, 'email username firstName lastName gender birthDate country city phone userRoles verified', function (err, user) {
            if (err) return console.log(err);
            res.json(user);
        });
    });
    app.get('/api/getCourseByID', function(req, res) {
        let id = url.parse(req.url, true).query.id;
        Course.findById(id, 'title description isSelfStudy', function (err, course) {
            if (err) return console.log(err);
            res.json(course);
        });
    });
    app.get('/api/getCourseTeachersByID', function(req, res) {
        let courseId = url.parse(req.url, true).query.id;
        // User.find({
        //     teacherCourses: courseId
        // }, 'lastName firstName', function (err, courseTeachers) {
        //     if (err) return console.log(err);
        //     res.json(courseTeachers);
        Course.findById(courseId, 'teachers', function (err, course) {
            if (err) return console.log(err);
            console.log(course);
            res.send(course.teachers);
            //res.json(courseTeachers);
        });
    });
    app.get('/api/getTeacherCourses', function(req, res) {
        let userId = url.parse(req.url, true).query.id;
        Course.find({ teachRequests: userId }, 'title',function (err, courses) {
            if (err) return console.log(err);
            console.log(courses);
            res.send(courses);
        });

        // User.findById(userId, 'teacherCourses', function (err, user) {
        //     if (err) return console.log(err);
        //     Course.find({
        //         '_id': { $in: user.teacherCourses}
        //     }, function(err, courses){
        //         res.send(courses);
        //     });
        // });
    });
    app.post('/api/teachRequest', function(req, res) {
        // User.findById(req.body.userId, function (err, user) {
        //     if (err) return console.log(err);
        //     if (user.teacherCourses.indexOf(req.body.courseId) == -1) {
        //         user.teacherCourses.push(req.body.courseId);
        //         user.save(function (err, updatedUser) {
        //             if (err) return console.log(err);
        //             res.send(updatedUser);
        //         });
        //     } else {
        //         res.send('Course already teaching');
        //     }
        // });
        let payload = req.body;
        console.log(payload);
        Course.findById(payload.courseId, 'teachRequests', function (err, course) {
            if (err) return console.log(err);
            console.log(course);
            course.teachRequests.push(payload.userId);
            course.save(function (err, updatedCourse) {
                if (err) return console.log(err);
                res.send('Success');
            });
        });
    });
    app.put('/api/studyRequest', function(req, res) {
        let payload = req.body;
        console.log(payload);
        Course.findById(payload.courseId, 'requests', function (err, course) {
            if (err) return console.log(err);
            console.log('studyRequest');
            console.log(course);
            let request = {
                userId: payload.userId,
                teacherId: payload.teacherId,
                isSelfStudy: payload.isSelfStudy
            };
            course.studyRequests.push(request);

            course.save(function (err, updatedCourse) {
                if (err) return console.log(err);
                res.send('Success');
            });
        });
    });

    app.post('/api/studyCourse', function(req, res) {
        let userId = jwt.decode(req.headers.authorization.slice(4), cfg.jwtSecret).id; //пиздец какой костыль, но нет времени разбираться с авторизацией
        console.log(userId);
        console.log(req.body);
        User.findById(userId, function (err, user) {
            if (err) return console.log(err);
            console.log(user.courses.studying);
            user.courses.studying.push(req.body.courseId);
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                console.log(updatedUser.courses.studying);
                res.status(200).send('OK');
            });
        });
    });

    app.post('/api/addParentInfo', function(req, res) {
        let newParent = req.body.parent;
        console.log(newParent);
        User.findById(req.body.userId, function (err, user) {
            if (err) return console.log(err);
            user.parents.push(newParent);
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser);
            });
        });
    });

    app.post('/api/addGameRecord', function(req, res) {
        console.log(req.body);
        User.findById(req.body.userId, function (err, user) {
            if (err) return console.log(err);
            console.log(user.mental.gameNumber[req.body.gameType]);
            user.mental.gameNumber[req.body.gameType] += 1;
            console.log(user.mental.gameNumber[req.body.gameType]);
            if (req.body.isCorrect) {
                user.mental.gameWins[req.body.gameType] += 1;
            } else if(!req.body.isCorrect && user.mental.gameWins[req.body.gameType] !== 0) {
                user.mental.gameWins[req.body.gameType] -= 1
            }
            console.log(user);
            user.save(function (err, updatedUser) {
                if (err) return console.log(err);
                res.send(updatedUser.mental);
            });
        });
    });

};