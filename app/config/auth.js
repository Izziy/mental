// auth.js
let passport = require("passport");
let passportJWT = require("passport-jwt");
let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;

let User  = require('../models/user');
let cfg = require("./config.js");

let params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    passReqToCallback : true
};

module.exports = function() {

    passport.use(new JwtStrategy(params, function(req, payload, done) {
        process.nextTick(function() {
            User.findOne({ '_id' : payload.id }, function(err, user) {
                if (err)
                    return done(err, false);
                if (user)
                    return done(null, user);
                else
                    return done(new Error("No user found."), false);
            });
        });
    }));

    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};