"use strict";

// auth.js
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var User = require('../models/user');
var cfg = require("./config.js");

var params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    passReqToCallback: true
};

module.exports = function () {

    passport.use(new JwtStrategy(params, function (req, payload, done) {
        process.nextTick(function () {
            User.findOne({ '_id': payload.id }, function (err, user) {
                if (err) return done(err, false);
                if (user) return done(null, user);else return done(new Error("No user found."), false);
            });
        });
    }));

    return {
        initialize: function initialize() {
            return passport.initialize();
        },
        authenticate: function authenticate() {
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};

//# sourceMappingURL=auth-compiled.js.map