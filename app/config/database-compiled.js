'use strict';

// config/database.js
module.exports = {

    url: {
        development: 'mongodb://localhost/development',
        test: 'mongodb://localhost/test'
    }, // looks like mongodb://<user>:<pass>@mongo.onmodulus.net:27017/Mikha4ot
    options: {
        server: {
            socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 }
        },
        replset: {
            socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 }
        }
    }
};

//# sourceMappingURL=database-compiled.js.map