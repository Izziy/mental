module.exports = {

    isLoggedIn: function (req, res, next) {
        if (req.isAuthenticated())
            return next();
        else
            res.send('FUCK OFF');
    },

    isAllowed: function (group) {
        return function(req, res, next) {
            if (req.user && req.user.groups.indexOf(group) > -1)
                next();
            else
                res.redirect('/');
        };
    }
};