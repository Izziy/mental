"use strict";

var express = require('express');
var path = require('path');
var app = express();

var history = require('connect-history-api-fallback');
//let passport     = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require("body-parser");
//let session      = require('express-session');
var auth = require("./app/config/auth")();

//require('./app/config/passport')(passport); // pass passport for configuration

var staticFileMiddleware = express.static(path.join(__dirname + '/public'));
// set up our express application
app.use(staticFileMiddleware);
app.use(history());
app.use(staticFileMiddleware);
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
//app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
/*app.use(session({
    secret: 'itstimetomakesomementaltraining', // session secret
    resave: true,
    saveUninitialized: true
}));*/
app.use(auth.initialize());
//app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes/index.js')(app); // load our routes and pass in our app and fully configured passport

module.exports = app;

//# sourceMappingURL=app-compiled.js.map