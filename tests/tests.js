"use strict";

let request = require('supertest');
let utils = require('./utils.js');
let app = require('./../app');


describe('Requests to the root path', () => {
    it('Returns a 200 status code', done => {
        request(app)
            .get('/')
            .expect(200)
            .end(error => {
                if(error) throw error;
                done();
            });
    });
    it('Returns a HTML format', done => {
        request(app)
            .get('/')
            .expect('Content-Type', /html/, done);
    });
});

describe('Creates new user', () => {

    it('Returns a 302 status code', done => {
        request(app)
            .post('/signup')
            .send({'email': 'Test123512354','password': 'PassMe'})
            .expect(302, done)
    });

    it('Redirects to /profile', done => {
        request(app)
            .post('/signup')
            .send({'email': 'Test','password': 'PassMe'})
            .expect('Content-Type', /plain/)
            .expect('Location', '/profile', done)
    });

    it('Sets cookie', done => {
        request(app)
            .post('/signup')
            .send({'email': 'Test','password': 'PassMe'})
            .expect('set-cookie', /connect.sid/, done)
    });

});

