"use strict";

var request = require('supertest');
var utils = require('./utils.js');
var app = require('./../app');

describe('Requests to the root path', function () {
    it('Returns a 200 status code', function (done) {
        request(app).get('/').expect(200).end(function (error) {
            if (error) throw error;
            done();
        });
    });
    it('Returns a HTML format', function (done) {
        request(app).get('/').expect('Content-Type', /html/, done);
    });
});

describe('Creates new user', function () {

    it('Returns a 302 status code', function (done) {
        request(app).post('/signup').send({ 'email': 'Test123512354', 'password': 'PassMe' }).expect(302, done);
    });

    it('Redirects to /profile', function (done) {
        request(app).post('/signup').send({ 'email': 'Test', 'password': 'PassMe' }).expect('Content-Type', /plain/).expect('Location', '/profile', done);
    });

    it('Sets cookie', function (done) {
        request(app).post('/signup').send({ 'email': 'Test', 'password': 'PassMe' }).expect('set-cookie', /connect.sid/, done);
    });
});

//# sourceMappingURL=tests-compiled.js.map