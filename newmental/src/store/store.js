import Vue from 'vue';
import Vuex from 'vuex';
import common from './modules/common';
import user from './modules/user';
import game from './modules/game';
import * as actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    common,
    user,
    game
  },
  actions,
  strict: true
});
