import Vue from 'vue'
import mental from '../../mentalFunctions';

const state = {
  game: {
    status: {
      numberSwitcher: false
    }
  },
  gameList: [
    {
      rus: 'Просто',
      eng: 'simple'
    },
    {
      rus: 'Братья',
      eng: 'brothers'
    },
    {
      rus: 'Друзья',
      eng: 'friends'
    },
    {
      rus: 'Анзан',
      eng: 'anzan'
    }]
};

const getters = {
  game: state => {
    return state.game;
  },
  gameList: state => {
    return state.gameList;
  },
  gameType: state => {
    return state.game.type;
  },
  gameTypeSys: state => {
    return state.game.typeSys;
  },
  numberMap: state => {
    return state.numberMap;
  },
  numberSwitcher: state => {
    return state.game.status.numberSwitcher
  },
  newNumber: state => {
    return state.game.status.nextNumber1
  },
  gameStarted: state => {
    return state.game.gameStarted
  }
};

const mutations = {
  'SELECT_GAME' (state, game) {
    state.game.type = game.rus;
    /*    if (game == 'Простой') {
     state.game.typeSys = 'simple';
     } else if (game == 'Братья'){
     state.game.typeSys = 'brothers';
     } else if (game == 'Друзья'){
     state.game.typeSys = 'friends';
     } else if (game == 'Анзан'){
     state.game.typeSys = 'anzan';
     }*/
    state.game.typeSys = game.eng;
  },
  'INIT_GAME' (state) {
    state.game = {
      type: null,
      typeSys: null,
      options: {
        passedBone: 9,   // изученное число, используется в простом
        brother: 4,      // состав числа 4, используется в братьях
        friend: 9,      // состав числа 10, используется в друзьях
        maxNumber: 9,
        difficulty: 9,
        maxCount: 5,
        delay: 1500,
        doubleMode: false
      },
      status: {
        countdown: 3,
        counter: 0,
        numberSwitcher: false,
        result: {
          result1: 0,
          result2: 0,
          resultSet1: [],
          resultSet2: []
        },
        nextNumber1: 0,
        nextNumber2: 0
      },
      sounds: [],
      message: '',
      gameStarted: false,
      gameFinished: false,
      isAnswered: false,
      isCorrect: false
    };
  },
  'START_GAME' (state) {
    state.game.gameStarted = true;
    state.game.gameFinished = false;
    state.game.isAnswered = false;
    state.game.isCorrect = false;
    state.game.answer1 = '';
    state.game.answer2 = '';
    state.game.status = {
      countdown: 3,
      counter: 0,
      numberSwitcher: false,
      result: {
        result1: 0,
        result2: 0,
        resultSet1: [],
        resultSet2: [],
      },
      nextNumber1: 0,
      nextNumber2: 0
    };

    let delay = state.game.options.delay;
    let difficulty = state.game.options.difficulty;
    let doubleMode = state.game.options.doubleMode;
    let maxCount = state.game.options.maxCount;
    let type = state.game.typeSys;

    let passedBone = state.game.options.passedBone;
    let brother = state.game.options.brother;
    let friend = state.game.options.friend;
    let modifier = null;
    if (type == 'simple') {
      modifier = passedBone;
    } else if (type == 'brothers') {
      modifier = brother;
    } else if (type == 'friends') {
      modifier = friend;
    } else if (type == 'anzan') {
      modifier = '9';
    }

    let res = mental.generateSet(difficulty, maxCount, type, modifier);
    state.game.status.result.result1 = res.result;
    state.game.status.result.resultSet1 = res.resultSet;

    // если двойной режим отключен, то начитываем необходимые для озвучки аудиофайлы
    if (!doubleMode && difficulty <= 999 && delay >= 1500) {
      ion.sound({
        sounds: res.resultSet.map((sound)=>{
          return {name: sound < 0 ? '-' + Math.abs(sound).toString() : '+' + sound.toString()};
        }),
        path: "../../static/sounds/",
        preload: true,
        multiplay: false,
        volume: 1
      });
    }

    state.game.sounds = res.resultSet.map((sound)=>{
      return sound < 0 ? '-' + Math.abs(sound).toString() : '+' + sound.toString();
    });

    if (doubleMode) {
      let res2 = mental.generateSet(difficulty, maxCount, type, modifier);
      state.game.status.result.result2 = res2.result;
      state.game.status.result.resultSet2 = res2.resultSet;
    }
  },
  'CHECK_ANSWER' (state, answers) {
    state.game.isAnswered = true;
    let answer1 = parseInt(answers.answer1);
    let answer2 = parseInt(answers.answer2);
    if (!state.game.options.doubleMode) {
      if (answer1 == state.game.status.result.result1) {
        state.game.message = 'Правильно!';
        state.game.isCorrect = true;
      } else {
        state.game.message = 'Неправильно!';
        state.game.isCorrect = false;
      }
    } else {
      if (answer1 == state.game.status.result.result1 && answer2 == state.game.status.result.result2) {
        state.game.message = 'Правильно!';
        state.game.isCorrect = true;
      } else if (answer1 != state.game.status.result.result1 && answer2 != state.game.status.result.result2) {
        state.game.message = 'Оба ответа неверны!';
        state.game.isCorrect = false;
      } else {
        state.game.message = '1 ответ не верен!';
        state.game.isCorrect = false;
      }
    }

  },
  'UPDATE_PASSEDBONE' (state, value) {
    state.game.options.passedBone = parseInt(value)
  },
  'UPDATE_BROTHER' (state, value) {
    state.game.options.brother = parseInt(value)
  },
  'UPDATE_FRIEND' (state, value) {
    state.game.options.friend = parseInt(value)
  },
  'UPDATE_DIFFICULTY' (state, value) {
    state.game.options.difficulty = parseInt(value)
  },
  'UPDATE_MAXCOUNT' (state, value) {
    state.game.options.maxCount = parseInt(value)
  },
  'UPDATE_DELAY' (state, value) {
    state.game.options.delay = parseInt(value)
  },
  'UPDATE_DOUBLEMODE' (state, value) {
    state.game.options.doubleMode = value
  },
  'DECREMENT_COUNTDOWN' (state) {
    state.game.status.countdown -= 1;
  },
  'INCREMENT_COUNTER' (state) {
    state.game.status.counter++;
  },
  'NEXT_NUMBER' (state) {
    let counter = state.game.status.counter;
    let delay = state.game.options.delay;
    let difficulty = state.game.options.difficulty;
    let doubleMode = state.game.options.doubleMode;
    let result = state.game.status.result;
    if (state.game.sounds.length > counter && !doubleMode && difficulty <= 999 && delay >= 1500) {
      ion.sound.play(state.game.sounds[counter])
    }
    state.game.status.numberSwitcher = !state.game.status.numberSwitcher;
    // console.log(state.game.status.countdown == 0 && !state.game.status.numberSwitcher);

    state.game.status.nextNumber1 = result.resultSet1[counter];
    state.game.status.nextNumber2 = result.resultSet2[counter];

    // console.log(state.game.status.countdown == 0 && !state.game.status.numberSwitcher);
  },
  'FINISH_GAME' (state) {
    state.game.gameStarted = false;
    state.game.gameFinished = true;
  }
};

const actions = {
  selectGame: ({commit}, game) => {
    commit('SELECT_GAME', game);
  },
  initGame: ({commit}) => {
    commit('INIT_GAME');
  },
  startGame: ({commit}) => {
    commit('START_GAME');
    /*let startInterval = setInterval(() => {

     commit('DECREMENT_COUNTDOWN');
     if (state.game.status.countdown == 0) {

     clearInterval(startInterval);
     commit('NEXT_NUMBER');

     let taskInterval = setInterval(()=> {

     commit('INCREMENT_COUNTER');
     commit('NEXT_NUMBER');

     if (state.game.status.counter == state.game.status.result.resultSet1.length) {
     clearInterval(taskInterval);
     commit('FINISH_GAME');
     }

     }, state.game.options.delay);

     }
     },1000);*/
    let countdownTimer = setTimeout(function tick() {

      commit('DECREMENT_COUNTDOWN');

      if (state.game.status.countdown == 0) {
        clearTimeout(countdownTimer);
        startTaskTimer();
      } else {
        countdownTimer = setTimeout(tick, 1000);
      }

    }, 1000);

    function startTaskTimer() {

      commit('NEXT_NUMBER');

      let taskTimer = setTimeout(function task() {

        commit('INCREMENT_COUNTER');
        commit('NEXT_NUMBER');
        if (state.game.status.counter === state.game.status.result.resultSet1.length) {
          clearTimeout(taskTimer);
          commit('FINISH_GAME');
        } else {
          taskTimer = setTimeout(task, state.game.options.delay);
        }

      }, state.game.options.delay);

    }
  },

  checkAnswer: ({commit, rootState}, answers) => {
    commit('CHECK_ANSWER', answers);

    let payload = {
      userId: rootState.user.user._id,
      gameType: state.game.typeSys,
      isCorrect: state.game.isCorrect
    };
    Vue.http.post('/api/addGameRecord', payload).then((data) => {
      console.log('Success', data);
    }, (error) => {
      console.log('Error', error);
    });

  },
  showAnswer: () => {

  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
