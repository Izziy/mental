const state = {
  drawer: true
};

const getters = {
  drawer: state => {
    return state.drawer;
  }
};

const mutations = {
  'SET_DRAWER' (state) {
    state.drawer = !state.drawer;
  }
};

const actions = {
  setDrawer: ({commit}) => {
    commit('SET_DRAWER');
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
