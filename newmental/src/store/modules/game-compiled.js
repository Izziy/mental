'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var state = {
  game: {
    options: {
      passedBone: 9,
      maxNumber: 9,
      maxCount: 5,
      delay: 500,
      doubleMode: false
    },
    status: {
      countdown: 3,
      counter: 0,
      result: {
        result1: 0,
        result2: 0,
        resultSet1: [],
        resultSet2: []
      },
      nextNumber1: 0,
      nextNumber2: 0
    },
    answer1: '',
    answer2: '',
    message: '',
    gameStarted: false,
    gameFinished: false,
    isAnswered: false,
    isCorrect: false
  }
};

var getters = {
  user: function user(state) {
    return state.user;
  }
};

var mutations = {};

var actions = {};

exports.default = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
};

//# sourceMappingURL=game-compiled.js.map