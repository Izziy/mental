import Vue from 'vue'
import router from '../../router'

const state = {
  user: {
    courses: {studying: []},
    userRoles: [],

    mental: {
      gameNumber: {
      },
      gameWins: {
      },
      homeWorks: []
    }
  },
  idToken: null,
  authMessage: {
    success: true,
    message: null
  }
};

const getters = {
  authMessage: state => {
    return state.authMessage
  },
  user: state => {
    return state.user
  },
  userId: state => {
    return state.user._id
  },
  fullName: state => {
    return state.user.firstName + ' ' + state.user.lastName
  },
  isAuthenticated: state => {
    return !!state.idToken
  },
  isVerified: state => {
    if (state.user.verified) return state.user.verified.personalInfo
  },
  isTeacher: state => {
    return state.user.userRoles.some(role => role == 'teacher')
  },
  myCourses: state => {
    return state.user.courses.studying.map(course => course._id)
  }
};

const mutations = {
  'CLEAR_STATE' (state) {
    state.user = {
      courses: {studying: []},
      userRoles: [],

        mental: {
        gameNumber: {
        },
        gameWins: {
        },
        homeWorks: []
      }
    };
    state.idToken = null
  },
  'SET_USER' (state, user) {
    state.user = user;
  },
  'SET_TOKEN' (state, token) {
    state.idToken = token;
  },
  'SET_AUTH_MESSAGE' (state, message) {
    state.authMessage = message;
  },
  'CLEAR_AUTH_MESSAGE' (state) {
    state.authMessage = {
      success: true,
      message: null
    };
  },
  'ADD_PARENT_INFO' (state, parent) {
    state.user.parents.push(parent);
    console.log(parent);
    let payload = {
      userId: state.user._id,
      parent: parent
    };
    Vue.http.post('/api/addParentInfo', payload).then((data) => {
      console.log('Success', data);
    }, (error) => {
      console.log('Error', error);
    });
  },
  'STUDY_COURSE' (state, course) {
    state.user.courses.studying.push(course);
  },
};

const actions = {
  initState: ({commit}) => {
    commit('CLEAR_STATE');
  },
  // id пользователя берется с токена
  initUserInfo: ({commit}) => {
    Vue.http.get('/api/initUserInfo')
      .then((response) => {
        Promise.resolve(response.data);
        commit('SET_USER', response.data);
      })
      .catch((error) => {
        console.log(error);
        Promise.reject(error)
      });
  },
  setToken: ({commit}, token) => {
    commit('SET_TOKEN', token);
  },
  login: ({commit,getters}, credentials, redirect) => {
    Vue.http.post('/api/login', credentials)
      .then((response) => {
        Promise.resolve(response.data);
        let token = response.data.id_token;
        let user = response.data.user;

        localStorage.setItem('id_token', token);
        commit('SET_TOKEN', token);
        commit('SET_USER', user);

        if(redirect) {
          router.push(redirect);
        } else {
          router.push({name: 'User',params: {id: getters.userId}});
        }
      })
      .catch((error) => {
        let message = {
          success: false,
          message: error.data
        };
        commit('SET_AUTH_MESSAGE', message);
        console.log(error);
        Promise.reject(error);
      })
  },
  signup: ({commit, getters}, credentials, redirect) => {
    Vue.http.post('/api/signup', credentials)
      .then((response) => {
        let message = {
          success: true,
          message: response.data
        };
        commit('SET_AUTH_MESSAGE', message);
        // localStorage.setItem('id_token', response.data.id_token);

/*        if(redirect) {
          router.push(redirect);
        } else {
          router.push({name: 'Home',params: {id: getters.userId}});
        }*/

      })
      .catch((error) => {
        let message = {
          success: false,
          message: error.data
        };
        commit('SET_AUTH_MESSAGE', message);
        Promise.reject(error);
    })
  },
  logout: ({commit}) => {
    localStorage.removeItem('id_token');
    commit('CLEAR_STATE');
    router.push({name: 'Home'});
  },
  addParentInfo: ({commit}, parent) => {
    commit('ADD_PARENT_INFO', parent);
  },
  studyCourse: ({commit, getters}, course) => {
    if (getters.myCourses.indexOf(course._id) == -1) {
      Vue.http.post('/api/studyCourse', { courseId: course._id})
        .then((response) => {
          console.log(response);
          Promise.resolve(response.data);
          commit('STUDY_COURSE', course);
        })
        .catch((error) => {
          console.log(error);
          Promise.reject(error)
        });
    } else {
      console.log('Вы уже записаны на курс')
    }
  },
  teachRequest: ({commit}, courseId) => {
    Vue.http.post('/api/postTeachCourseRequest', {courseId: courseId})
      .then((response) => {
        console.log(response);
        Promise.resolve(response.data)
      })
      .catch((error) => {
        console.log(error);
        Promise.reject(error)
      });
/*
      .then(response => {
        console.log('я тута');
        console.log(response);
      })
      .then(error => {
        if(error) {
          console.log('error');
          console.log(response.body);
        }
      })*/
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
