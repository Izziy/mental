import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import auth from '@/auth/auth.js';
import Home from '@/components/Home.vue';
import Login from '@/components/Login.vue';
import Signup from '@/components/Signup.vue';
import Feedback from '@/components/Feedback.vue';
import Platform from '@/components/Platform.vue';
import User from '@/components/platform/user/User.vue';
import UserOptions from '@/components/platform/user/UserOptions.vue';
import UserOptionsPrimary from '@/components/platform/user/UserOptionsPrimary.vue';
import UserOptionsSecurity from '@/components/platform/user/UserOptionsSecurity.vue';
import UserOptionsExtra from '@/components/platform/user/UserOptionsExtra.vue';
import UserFriends from '@/components/platform/user/Users.vue';
import Admin from '@/components/admin/Admin.vue';
import AdminDashboard from '@/components/admin/Dashboard.vue';
import AdminUsers from '@/components/admin/Users.vue';
import AdminCourses from '@/components/admin/Courses.vue';
import AdminTeachRequests from '@/components/admin/TeachRequests.vue';
import Courses from '@/components/courses/Courses.vue';
import CourseWrapper from '@/components/courses/CourseWrapper.vue';
import Teacher from '@/components/teacher/Teacher.vue';
import TeacherDashboard from '@/components/teacher/Dashboard.vue';
import TeacherCourses from '@/components/teacher/Courses.vue';
import TeacherCourse from '@/components/teacher/Course.vue';
import TeacherMental from '@/components/teacher/Mental.vue';
import TeacherStudents from '@/components/teacher/Students.vue';
import Student from '@/components/student/Student.vue';
import StudentDashboard from '@/components/student/Dashboard.vue';
import StudentCourses from '@/components/student/Courses.vue';
import StudentCourse from '@/components/student/Course.vue';
import StudentMental from '@/components/student/Mental.vue';
import Game from '@/components/platform/courses/mental/game/Game.vue';
import MyCourse from '@/components/platform/courses/MyCourse.vue';
import MyCoursePrimary from '@/components/platform/courses/MyCoursePrimary.vue';
import MyCourseMental from '@/components/platform/courses/mental/MyCourseMental.vue';
import MyCourseMentalPrimary from '@/components/platform/courses/mental/MyCourseMentalPrimary.vue';
import MyCourseMentalSecondary from '@/components/platform/courses/mental/MyCourseMentalSecondary.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    { path: '/signup',
      name: 'Signup',
      component: Signup
    },
    { path: '/platform',
      name: 'Platform',
      component: Platform,
      // beforeEnter: auth.requireAuth,
      meta: { auth: true },
      children: [
        {
          path: 'user/:id',
          name: 'User',
          component: User
        },
        {
          path: 'options',
          component: UserOptions,
          children: [
            {
              path: 'primary',
              name: 'Options',
              component: UserOptionsPrimary
            },
            {
              path: 'security',
              name: 'OptionsSecurity',
              component: UserOptionsSecurity
            },
            {
              path: 'extra',
              name: 'OptionsExtra',
              component: UserOptionsExtra
            }
          ]
        },
        {
          path: 'my-courses/mental',
          name: 'MyCourseMental',
          component: MyCourseMental,
          children: [
            {
              path: 'primary',
              name: 'MyCourseMentalPrimary',
              component: MyCourseMentalPrimary
            },
            {
              path: 'secondary',
              name: 'MyCourseMentalSecondary',
              component: MyCourseMentalSecondary
            },
            {
              path: 'game/:name',
              name: 'Game',
              component: Game
            },
          ]
        },
        { path: 'my-courses/:id',
          name: 'MyCourse',
          component: MyCourse,
          children: [
            {
              path: 'primary',
              name: 'MyCoursePrimary',
              component: MyCoursePrimary
            }
          ]
        },
        { path: 'courses',
          name: 'Courses',
          component: Courses
        },
        {
          path: 'courses/:id',
          name: 'Course',
          component: CourseWrapper
        },
        {
          path: 'users',
          name: 'Friends',
          component: UserFriends
        },
        {
          path: 'teacher',
          name: 'Teacher',
          component: Teacher,
          beforeEnter: auth.requireTeacher,
          children: [
            {
              path: 'dashboard',
              name: 'TeacherDashboard',
              component: TeacherDashboard
            },
            {
              path: 'courses',
              name: 'TeacherCourses',
              component: TeacherCourses
            },
            {
              path: 'courses/:id',
              name: 'TeacherCourse',
              component: TeacherCourse
            },
            {
              path: 'courses/mental',
              name: 'TeacherMental',
              component: TeacherMental
            },
            {
              path: 'students',
              name: 'TeacherStudents',
              component: TeacherStudents
            }
          ]
        },
        {
          path: 'student',
          name: 'Student',
          component: Student,
          children: [
            {
              path: 'dashboard',
              name: 'StudentDashboard',
              component: StudentDashboard
            },
            {
              path: 'courses',
              name: 'StudentCourses',
              component: StudentCourses
            },
            {
              path: 'courses/:id',
              name: 'StudentCourse',
              component: StudentCourse
            },
            {
              path: 'courses/mental',
              name: 'StudentMental',
              component: StudentMental
            }
          ]
        },
        { path: '/feedback',
          name: 'Feedback',
          component: Feedback
        },
        { path: 'admin',
          name: 'Admin',
          component: Admin,
          // beforeEnter: auth.requireAdmin,
          meta: { admin: true },
          children: [
            {
              path: 'dashboard',
              name: 'AdminDashboard',
              component: AdminDashboard
            },
            {
              path: 'users',
              name: 'AdminUsers',
              component: AdminUsers
            },
            {
              path: 'courses',
              name: 'AdminCourses',
              component: AdminCourses
            },
/*            {
              path: 'teachrequests',
              name: 'AdminTeachRequests',
              component: AdminTeachRequests
            }*/
          ]
        },
      ]
    },
  ],
  mode: 'history',
});

