// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueResource from 'vue-resource'
import App from './App';
import router from './router';
import store from './store/store'
import auth from './auth/auth.js';
import ion from 'ion-sound';
import 'animate.css'

Vue.use(Vuetify);
Vue.use(VueResource);
Vue.config.productionTip = false;
Vue.http.headers.common['Authorization'] = auth.getAuthHeader();
Vue.filter('pretty', (value) => JSON.stringify(value, null, 2));

/*router.beforeEach( (to,from,next) => {
  if (!store.getters.user.authenticated) {
    if (localStorage.user && localStorage.id_token) {
      let user = JSON.parse(localStorage.user);
      store.dispatch('setUser',user);
      store.dispatch('authenticateUser');
    }
    if (to.matched.some(record => record.meta.auth) && !store.getters.user.authenticated) {
      next('login');
    } else {
      next()
    }
  } else {
    next()
  }
});*/

router.beforeEach( (to,from,next) => {

  let token = localStorage.id_token;
  if (token && !store.getters.isAuthenticated) {
    // console.log('Токен найден, загружаю личные данные!');
    store.dispatch('setToken', token);
    store.dispatch('initUserInfo');
    // console.log('Вы успешно авторизованы.');
  }
  if (to.matched.some(record => record.meta.auth)) {
    if (!store.getters.isAuthenticated) {
      // console.log('Для этого роута нужна авторизация! Перенаправляю на страницу входа!');
      next('login');
    } else {
      if (to.matched.some(record => record.meta.admin)) {
        // console.log('Для этого роута нужны права админа!');
        // здесь необходимо добавить логику определения админа и редирект
        next()
      } else {
        // console.log('Пользователь авторизован! Допуск разрешен.');
        next()
      }
    }
  } else {
    next()
  }

  // console.log('Попытка перейти по роуту: ' + to.name);
  // console.log(to);

});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
