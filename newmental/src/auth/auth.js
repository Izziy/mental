import store from '../store/store'
import router from '../router'

// URL and endpoint constants
const API_URL = '/api';
const LOGIN_URL = API_URL + '/login';
const SIGNUP_URL = API_URL + '/signup';

export default {

  // Send a request to the login URL and save the returned JWT
  login(context, creds, redirect) {
    context.$http.post(LOGIN_URL, creds).then((data) => {
      localStorage.setItem('id_token', data.body.id_token);
      store.dispatch('setToken',data.body.id_token);
      store.dispatch('initUserInfo');
      if(redirect) {
        router.push(redirect);
      } else {
        router.push({name: 'User',params: {id: store.getters.userId}});
      }
    }, (err) => {
      context.error = err;
      console.log(err);
    });
  },

  signup(context, creds, redirect) {
    context.$http.post(SIGNUP_URL, creds, (data) => {
      localStorage.setItem('id_token', data.id_token);

      if(redirect) {
        context.$router.go(redirect)
      }

    }).error((err) => {
      context.error = err
    })
  },

  // The object to be passed as a header for authenticated requests
  getAuthHeader() {
    return 'JWT ' + localStorage.getItem('id_token')
  },

  requireTeacher (to, from, next) {
    console.log('requireTeacher executed!');
    console.log(store.getters.isAuthenticated);
    if (!store.getters.isAuthenticated) {
      next({ path: '/login' });
    } else if (store.getters.isAuthenticated && store.getters.user.userRoles.indexOf("teacher") != -1){
      next();
    }
  },
  requireAdmin (to, from, next) {
    console.log('requireAdmin executed!');
    console.log(store.getters.isAuthenticated);
    if (!store.getters.isAuthenticated) {
      next({ path: '/login' });
    } else if (store.getters.isAuthenticated && store.getters.user.userRoles.indexOf("admin") != -1){
      next();
    }
  },
  requireAuth (to, from, next) {
    console.log('requireAuth executed!');
    console.log(store.getters.isAuthenticated);
    console.log(to);
    if (!store.getters.isAuthenticated) {
      next({ path: '/login' });
    } else {
      next();
    }
  }
}
